# WordPress MySQL database migration
#
# Generated: Thursday 20. September 2018 08:47 UTC
# Hostname: localhost
# Database: `aeg`
# URL: https://www.aegvipstore.it
# Path: /home/runcloud/webapps/aegvipstore
# Tables: wp_commentmeta, wp_comments, wp_links, wp_options, wp_postmeta, wp_posts, wp_term_relationships, wp_term_taxonomy, wp_termmeta, wp_terms, wp_usermeta, wp_users
# Table Prefix: wp_
# Post Types: revision, page, post, wpsl_stores
# Protocol: http
# Multisite: false
# Subsite Export: false
# --------------------------------------------------------

/*!40101 SET NAMES utf8mb4 */;

SET sql_mode='NO_AUTO_VALUE_ON_ZERO';



#
# Delete any existing table `wp_commentmeta`
#

DROP TABLE IF EXISTS `wp_commentmeta`;


#
# Table structure of table `wp_commentmeta`
#

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_commentmeta`
#

#
# End of data contents of table `wp_commentmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_comments`
#

DROP TABLE IF EXISTS `wp_comments`;


#
# Table structure of table `wp_comments`
#

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_comments`
#
INSERT INTO `wp_comments` ( `comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Un commentatore di WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-09-19 17:59:37', '2018-09-19 15:59:37', 'Ciao, questo è un commento.\nPer iniziare a moderare, modificare ed eliminare commenti, visita la schermata commenti nella bacheca.\nGli avatar di chi lascia un commento sono forniti da <a href="https://it.gravatar.com">Gravatar</a>.', 0, '1', '', '', 0, 0) ;

#
# End of data contents of table `wp_comments`
# --------------------------------------------------------



#
# Delete any existing table `wp_links`
#

DROP TABLE IF EXISTS `wp_links`;


#
# Table structure of table `wp_links`
#

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_links`
#

#
# End of data contents of table `wp_links`
# --------------------------------------------------------



#
# Delete any existing table `wp_options`
#

DROP TABLE IF EXISTS `wp_options`;


#
# Table structure of table `wp_options`
#

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_options`
#
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'https://www.aegvipstore.it', 'yes'),
(2, 'home', 'https://www.aegvipstore.it', 'yes'),
(3, 'blogname', 'AEG', 'yes'),
(4, 'blogdescription', 'Un nuovo sito targato WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'andre.ciccu@softfobia.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j F Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j F Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/index.php/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:108:{s:11:"^wp-json/?$";s:22:"index.php?rest_route=/";s:14:"^wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:21:"^index.php/wp-json/?$";s:22:"index.php?rest_route=/";s:24:"^index.php/wp-json/(.*)?";s:33:"index.php?rest_route=/$matches[1]";s:24:"index.php/html5-blank/?$";s:31:"index.php?post_type=html5-blank";s:54:"index.php/html5-blank/feed/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=html5-blank&feed=$matches[1]";s:49:"index.php/html5-blank/(feed|rdf|rss|rss2|atom)/?$";s:48:"index.php?post_type=html5-blank&feed=$matches[1]";s:41:"index.php/html5-blank/page/([0-9]{1,})/?$";s:49:"index.php?post_type=html5-blank&paged=$matches[1]";s:57:"index.php/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:52:"index.php/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:52:"index.php?category_name=$matches[1]&feed=$matches[2]";s:33:"index.php/category/(.+?)/embed/?$";s:46:"index.php?category_name=$matches[1]&embed=true";s:45:"index.php/category/(.+?)/page/?([0-9]{1,})/?$";s:53:"index.php?category_name=$matches[1]&paged=$matches[2]";s:27:"index.php/category/(.+?)/?$";s:35:"index.php?category_name=$matches[1]";s:54:"index.php/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:49:"index.php/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?tag=$matches[1]&feed=$matches[2]";s:30:"index.php/tag/([^/]+)/embed/?$";s:36:"index.php?tag=$matches[1]&embed=true";s:42:"index.php/tag/([^/]+)/page/?([0-9]{1,})/?$";s:43:"index.php?tag=$matches[1]&paged=$matches[2]";s:24:"index.php/tag/([^/]+)/?$";s:25:"index.php?tag=$matches[1]";s:55:"index.php/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:50:"index.php/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?post_format=$matches[1]&feed=$matches[2]";s:31:"index.php/type/([^/]+)/embed/?$";s:44:"index.php?post_format=$matches[1]&embed=true";s:43:"index.php/type/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?post_format=$matches[1]&paged=$matches[2]";s:25:"index.php/type/([^/]+)/?$";s:33:"index.php?post_format=$matches[1]";s:47:"index.php/html5-blank/.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:57:"index.php/html5-blank/.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:77:"index.php/html5-blank/.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"index.php/html5-blank/.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:72:"index.php/html5-blank/.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:53:"index.php/html5-blank/.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:36:"index.php/html5-blank/(.+?)/embed/?$";s:44:"index.php?html5-blank=$matches[1]&embed=true";s:40:"index.php/html5-blank/(.+?)/trackback/?$";s:38:"index.php?html5-blank=$matches[1]&tb=1";s:60:"index.php/html5-blank/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?html5-blank=$matches[1]&feed=$matches[2]";s:55:"index.php/html5-blank/(.+?)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?html5-blank=$matches[1]&feed=$matches[2]";s:48:"index.php/html5-blank/(.+?)/page/?([0-9]{1,})/?$";s:51:"index.php?html5-blank=$matches[1]&paged=$matches[2]";s:55:"index.php/html5-blank/(.+?)/comment-page-([0-9]{1,})/?$";s:51:"index.php?html5-blank=$matches[1]&cpage=$matches[2]";s:44:"index.php/html5-blank/(.+?)(?:/([0-9]+))?/?$";s:50:"index.php?html5-blank=$matches[1]&page=$matches[2]";s:12:"robots\\.txt$";s:18:"index.php?robots=1";s:48:".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$";s:18:"index.php?feed=old";s:20:".*wp-app\\.php(/.*)?$";s:19:"index.php?error=403";s:18:".*wp-register.php$";s:23:"index.php?register=true";s:42:"index.php/feed/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:37:"index.php/(feed|rdf|rss|rss2|atom)/?$";s:27:"index.php?&feed=$matches[1]";s:18:"index.php/embed/?$";s:21:"index.php?&embed=true";s:30:"index.php/page/?([0-9]{1,})/?$";s:28:"index.php?&paged=$matches[1]";s:37:"index.php/comment-page-([0-9]{1,})/?$";s:38:"index.php?&page_id=9&cpage=$matches[1]";s:51:"index.php/comments/feed/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:46:"index.php/comments/(feed|rdf|rss|rss2|atom)/?$";s:42:"index.php?&feed=$matches[1]&withcomments=1";s:27:"index.php/comments/embed/?$";s:21:"index.php?&embed=true";s:54:"index.php/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:49:"index.php/search/(.+)/(feed|rdf|rss|rss2|atom)/?$";s:40:"index.php?s=$matches[1]&feed=$matches[2]";s:30:"index.php/search/(.+)/embed/?$";s:34:"index.php?s=$matches[1]&embed=true";s:42:"index.php/search/(.+)/page/?([0-9]{1,})/?$";s:41:"index.php?s=$matches[1]&paged=$matches[2]";s:24:"index.php/search/(.+)/?$";s:23:"index.php?s=$matches[1]";s:57:"index.php/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:52:"index.php/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:50:"index.php?author_name=$matches[1]&feed=$matches[2]";s:33:"index.php/author/([^/]+)/embed/?$";s:44:"index.php?author_name=$matches[1]&embed=true";s:45:"index.php/author/([^/]+)/page/?([0-9]{1,})/?$";s:51:"index.php?author_name=$matches[1]&paged=$matches[2]";s:27:"index.php/author/([^/]+)/?$";s:33:"index.php?author_name=$matches[1]";s:79:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:74:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:80:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]";s:55:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$";s:74:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true";s:67:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]";s:49:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$";s:63:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]";s:66:"index.php/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:61:"index.php/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$";s:64:"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]";s:42:"index.php/([0-9]{4})/([0-9]{1,2})/embed/?$";s:58:"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true";s:54:"index.php/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]";s:36:"index.php/([0-9]{4})/([0-9]{1,2})/?$";s:47:"index.php?year=$matches[1]&monthnum=$matches[2]";s:53:"index.php/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:48:"index.php/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$";s:43:"index.php?year=$matches[1]&feed=$matches[2]";s:29:"index.php/([0-9]{4})/embed/?$";s:37:"index.php?year=$matches[1]&embed=true";s:41:"index.php/([0-9]{4})/page/?([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&paged=$matches[2]";s:23:"index.php/([0-9]{4})/?$";s:26:"index.php?year=$matches[1]";s:68:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:78:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:98:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:93:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:93:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:74:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:63:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$";s:91:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true";s:67:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$";s:85:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1";s:87:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:82:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]";s:75:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]";s:82:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$";s:98:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]";s:71:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$";s:97:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]";s:57:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:67:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:87:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:82:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:82:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:63:"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:74:"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:81:"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]";s:61:"index.php/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$";s:65:"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]";s:48:"index.php/([0-9]{4})/comment-page-([0-9]{1,})/?$";s:44:"index.php?year=$matches[1]&cpage=$matches[2]";s:37:"index.php/.?.+?/attachment/([^/]+)/?$";s:32:"index.php?attachment=$matches[1]";s:47:"index.php/.?.+?/attachment/([^/]+)/trackback/?$";s:37:"index.php?attachment=$matches[1]&tb=1";s:67:"index.php/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"index.php/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$";s:49:"index.php?attachment=$matches[1]&feed=$matches[2]";s:62:"index.php/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$";s:50:"index.php?attachment=$matches[1]&cpage=$matches[2]";s:43:"index.php/.?.+?/attachment/([^/]+)/embed/?$";s:43:"index.php?attachment=$matches[1]&embed=true";s:26:"index.php/(.?.+?)/embed/?$";s:41:"index.php?pagename=$matches[1]&embed=true";s:30:"index.php/(.?.+?)/trackback/?$";s:35:"index.php?pagename=$matches[1]&tb=1";s:50:"index.php/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:45:"index.php/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$";s:47:"index.php?pagename=$matches[1]&feed=$matches[2]";s:38:"index.php/(.?.+?)/page/?([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&paged=$matches[2]";s:45:"index.php/(.?.+?)/comment-page-([0-9]{1,})/?$";s:48:"index.php?pagename=$matches[1]&cpage=$matches[2]";s:34:"index.php/(.?.+?)(?:/([0-9]+))?/?$";s:47:"index.php?pagename=$matches[1]&page=$matches[2]";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:3:{i:0;s:31:"wp-migrate-db/wp-migrate-db.php";i:1;s:45:"wp-store-locator-csv/wp-store-locator-csv.php";i:2;s:37:"wp-store-locator/wp-store-locator.php";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'html5blank', 'yes'),
(41, 'stylesheet', 'html5blank', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:"title";s:0:"";s:5:"count";i:0;s:12:"hierarchical";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Europe/Rome', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '9', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:6:{s:13:"administrator";a:2:{s:4:"name";s:13:"Administrator";s:12:"capabilities";a:78:{s:13:"switch_themes";b:1;s:11:"edit_themes";b:1;s:16:"activate_plugins";b:1;s:12:"edit_plugins";b:1;s:10:"edit_users";b:1;s:10:"edit_files";b:1;s:14:"manage_options";b:1;s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:6:"import";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:8:"level_10";b:1;s:7:"level_9";b:1;s:7:"level_8";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;s:12:"delete_users";b:1;s:12:"create_users";b:1;s:17:"unfiltered_upload";b:1;s:14:"edit_dashboard";b:1;s:14:"update_plugins";b:1;s:14:"delete_plugins";b:1;s:15:"install_plugins";b:1;s:13:"update_themes";b:1;s:14:"install_themes";b:1;s:11:"update_core";b:1;s:10:"list_users";b:1;s:12:"remove_users";b:1;s:13:"promote_users";b:1;s:18:"edit_theme_options";b:1;s:13:"delete_themes";b:1;s:6:"export";b:1;s:20:"manage_wpsl_settings";b:1;s:10:"edit_store";b:1;s:10:"read_store";b:1;s:12:"delete_store";b:1;s:11:"edit_stores";b:1;s:18:"edit_others_stores";b:1;s:14:"publish_stores";b:1;s:19:"read_private_stores";b:1;s:13:"delete_stores";b:1;s:21:"delete_private_stores";b:1;s:23:"delete_published_stores";b:1;s:20:"delete_others_stores";b:1;s:19:"edit_private_stores";b:1;s:21:"edit_published_stores";b:1;s:16:"wpsl_csv_manager";b:1;s:23:"wpsl_csv_manager_export";b:1;s:22:"wpsl_csv_manager_tools";b:1;}}s:6:"editor";a:2:{s:4:"name";s:6:"Editor";s:12:"capabilities";a:34:{s:17:"moderate_comments";b:1;s:17:"manage_categories";b:1;s:12:"manage_links";b:1;s:12:"upload_files";b:1;s:15:"unfiltered_html";b:1;s:10:"edit_posts";b:1;s:17:"edit_others_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:10:"edit_pages";b:1;s:4:"read";b:1;s:7:"level_7";b:1;s:7:"level_6";b:1;s:7:"level_5";b:1;s:7:"level_4";b:1;s:7:"level_3";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:17:"edit_others_pages";b:1;s:20:"edit_published_pages";b:1;s:13:"publish_pages";b:1;s:12:"delete_pages";b:1;s:19:"delete_others_pages";b:1;s:22:"delete_published_pages";b:1;s:12:"delete_posts";b:1;s:19:"delete_others_posts";b:1;s:22:"delete_published_posts";b:1;s:20:"delete_private_posts";b:1;s:18:"edit_private_posts";b:1;s:18:"read_private_posts";b:1;s:20:"delete_private_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"read_private_pages";b:1;}}s:6:"author";a:2:{s:4:"name";s:6:"Author";s:12:"capabilities";a:10:{s:12:"upload_files";b:1;s:10:"edit_posts";b:1;s:20:"edit_published_posts";b:1;s:13:"publish_posts";b:1;s:4:"read";b:1;s:7:"level_2";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;s:22:"delete_published_posts";b:1;}}s:11:"contributor";a:2:{s:4:"name";s:11:"Contributor";s:12:"capabilities";a:5:{s:10:"edit_posts";b:1;s:4:"read";b:1;s:7:"level_1";b:1;s:7:"level_0";b:1;s:12:"delete_posts";b:1;}}s:10:"subscriber";a:2:{s:4:"name";s:10:"Subscriber";s:12:"capabilities";a:2:{s:4:"read";b:1;s:7:"level_0";b:1;}}s:26:"wpsl_store_locator_manager";a:2:{s:4:"name";s:21:"Store Locator Manager";s:12:"capabilities";a:40:{s:4:"read";b:1;s:10:"edit_posts";b:1;s:12:"delete_posts";b:1;s:15:"unfiltered_html";b:1;s:12:"upload_files";b:1;s:19:"delete_others_pages";b:1;s:19:"delete_others_posts";b:1;s:12:"delete_pages";b:1;s:20:"delete_private_pages";b:1;s:20:"delete_private_posts";b:1;s:22:"delete_published_pages";b:1;s:22:"delete_published_posts";b:1;s:17:"edit_others_pages";b:1;s:17:"edit_others_posts";b:1;s:10:"edit_pages";b:1;s:18:"edit_private_pages";b:1;s:18:"edit_private_posts";b:1;s:20:"edit_published_pages";b:1;s:20:"edit_published_posts";b:1;s:17:"moderate_comments";b:1;s:13:"publish_pages";b:1;s:13:"publish_posts";b:1;s:18:"read_private_pages";b:1;s:18:"read_private_posts";b:1;s:10:"edit_store";b:1;s:10:"read_store";b:1;s:12:"delete_store";b:1;s:11:"edit_stores";b:1;s:18:"edit_others_stores";b:1;s:14:"publish_stores";b:1;s:19:"read_private_stores";b:1;s:13:"delete_stores";b:1;s:21:"delete_private_stores";b:1;s:23:"delete_published_stores";b:1;s:20:"delete_others_stores";b:1;s:19:"edit_private_stores";b:1;s:21:"edit_published_stores";b:1;s:16:"wpsl_csv_manager";b:1;s:23:"wpsl_csv_manager_export";b:1;s:22:"wpsl_csv_manager_tools";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'it_IT', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:"title";s:0:"";s:6:"number";i:5;}s:12:"_multiwidget";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:"title";s:0:"";s:5:"count";i:0;s:8:"dropdown";i:0;}s:12:"_multiwidget";i:1;}', 'yes') ;
INSERT INTO `wp_options` ( `option_id`, `option_name`, `option_value`, `autoload`) VALUES
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:"title";s:0:"";}s:12:"_multiwidget";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:4:{s:19:"wp_inactive_widgets";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:13:"widget-area-1";a:0:{}s:13:"widget-area-2";a:0:{}s:13:"array_version";i:3;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:"_multiwidget";i:1;}', 'yes'),
(112, 'cron', 'a:5:{i:1537433977;a:1:{s:34:"wp_privacy_delete_old_export_files";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:6:"hourly";s:4:"args";a:0:{}s:8:"interval";i:3600;}}}i:1537459177;a:3:{s:16:"wp_version_check";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:17:"wp_update_plugins";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}s:16:"wp_update_themes";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:10:"twicedaily";s:4:"args";a:0:{}s:8:"interval";i:43200;}}}i:1537459254;a:2:{s:19:"wp_scheduled_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}s:25:"delete_expired_transients";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}i:1537459787;a:1:{s:30:"wp_scheduled_auto_draft_delete";a:1:{s:32:"40cd750bba9870f18aada2478b24840a";a:3:{s:8:"schedule";s:5:"daily";s:4:"args";a:0:{}s:8:"interval";i:86400;}}}s:7:"version";i:2;}', 'yes'),
(113, 'theme_mods_twentyseventeen', 'a:2:{s:18:"custom_css_post_id";i:-1;s:16:"sidebars_widgets";a:2:{s:4:"time";i:1537374063;s:4:"data";a:4:{s:19:"wp_inactive_widgets";a:0:{}s:9:"sidebar-1";a:6:{i:0;s:8:"search-2";i:1;s:14:"recent-posts-2";i:2;s:17:"recent-comments-2";i:3;s:10:"archives-2";i:4;s:12:"categories-2";i:5;s:6:"meta-2";}s:9:"sidebar-2";a:0:{}s:9:"sidebar-3";a:0:{}}}}', 'yes'),
(132, 'can_compress_scripts', '1', 'no'),
(141, 'recently_activated', 'a:0:{}', 'yes'),
(142, 'wpsl_settings', 'a:89:{s:14:"api_server_key";s:39:"AIzaSyCi2dOr2y0i7vLVY78QIIrlPjgtNHFEy9s";s:15:"api_browser_key";s:39:"AIzaSyCi2dOr2y0i7vLVY78QIIrlPjgtNHFEy9s";s:12:"api_language";s:2:"it";s:10:"api_region";s:0:"";s:21:"api_geocode_component";i:0;s:12:"autocomplete";i:0;s:16:"results_dropdown";i:0;s:15:"radius_dropdown";i:0;s:15:"category_filter";i:0;s:20:"category_filter_type";s:8:"dropdown";s:13:"distance_unit";s:2:"km";s:11:"max_results";s:14:"[25],50,75,100";s:13:"search_radius";s:22:"10,25,[50],100,200,500";s:10:"zoom_level";i:3;s:15:"auto_zoom_level";s:2:"15";s:10:"start_name";s:4:"Roma";s:12:"start_latlng";s:21:"41.9027835,12.4963655";s:13:"run_fitbounds";i:1;s:8:"map_type";s:7:"roadmap";s:11:"auto_locate";i:1;s:8:"autoload";i:1;s:14:"autoload_limit";i:50;s:10:"streetview";i:0;s:12:"type_control";i:0;s:11:"scrollwheel";i:1;s:16:"control_position";s:4:"left";s:9:"map_style";s:2:"""";s:11:"template_id";s:7:"default";s:15:"marker_clusters";i:0;s:12:"cluster_zoom";i:0;s:12:"cluster_size";i:0;s:6:"height";s:3:"600";s:16:"infowindow_width";s:3:"500";s:12:"search_width";s:4:"100%";s:11:"label_width";s:2:"95";s:10:"new_window";i:1;s:9:"reset_map";i:0;s:23:"listing_below_no_scroll";i:0;s:18:"direction_redirect";i:1;s:9:"more_info";i:0;s:9:"store_url";i:1;s:9:"phone_url";i:1;s:17:"marker_streetview";i:0;s:14:"marker_zoom_to";i:0;s:11:"mouse_focus";i:1;s:20:"show_contact_details";i:1;s:25:"clickable_contact_details";i:1;s:12:"hide_country";i:0;s:13:"hide_distance";i:0;s:13:"marker_effect";s:6:"bounce";s:14:"address_format";s:14:"city_state_zip";s:18:"more_info_location";s:11:"info window";s:16:"infowindow_style";s:7:"default";s:12:"start_marker";s:14:"dark-green.png";s:12:"store_marker";s:14:"dark-green.png";s:14:"editor_country";s:0:"";s:15:"editor_map_type";s:7:"roadmap";s:10:"hide_hours";i:0;s:17:"editor_hour_input";s:8:"dropdown";s:18:"editor_hour_format";i:12;s:12:"editor_hours";a:1:{s:8:"dropdown";a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}}s:10:"permalinks";i:0;s:14:"permalink_slug";s:6:"stores";s:13:"category_slug";s:14:"store-category";s:12:"search_label";s:33:"Inserisci CAP, indirizzo o città";s:16:"search_btn_label";s:5:"Cerca";s:15:"preloader_label";s:26:"Ricerchiamo rivenditori...";s:12:"radius_label";s:0:"";s:16:"no_results_label";s:16:"Nessun risultato";s:13:"results_label";s:7:"Results";s:10:"more_label";s:9:"More info";s:16:"directions_label";s:19:"Ottieni indicazioni";s:19:"no_directions_label";s:58:"No route could be found between the origin and destination";s:10:"back_label";s:4:"Back";s:17:"street_view_label";s:11:"Street view";s:15:"zoom_here_label";s:9:"Zoom here";s:11:"error_label";s:39:"Something went wrong, please try again!";s:11:"phone_label";s:8:"Telefono";s:9:"fax_label";s:3:"Fax";s:11:"email_label";s:5:"Email";s:9:"url_label";s:4:"Sito";s:11:"hours_label";s:5:"Hours";s:11:"start_label";s:14:"Start location";s:11:"limit_label";s:23:"API usage limit reached";s:14:"category_label";s:15:"Category filter";s:22:"category_default_label";s:3:"Any";s:12:"show_credits";i:0;s:5:"debug";i:0;s:16:"deregister_gmaps";i:0;}', 'yes'),
(143, 'wpsl_version', '2.2.15', 'yes'),
(144, 'wpsl_csv_iso_fixed', '1', 'yes'),
(145, 'wpsl_csv_version', '1.2.0', 'yes'),
(148, 'current_theme', 'HTML5 Blank', 'yes'),
(149, 'theme_mods_html5blank', 'a:3:{i:0;b:0;s:18:"nav_menu_locations";a:0:{}s:18:"custom_css_post_id";i:-1;}', 'yes'),
(150, 'theme_switched', '', 'yes'),
(151, 'wpsl_valid_server_key', '1', 'yes'),
(171, 'wpsl_store_category_children', 'a:0:{}', 'yes'),
(172, 'wpsl_notices', 'a:0:{}', 'yes'),
(175, 'wpsl_delete_transient', '0', 'yes'),
(208, 'wpmdb_usage', 'a:2:{s:6:"action";s:8:"savefile";s:4:"time";i:1537433267;}', 'no') ;

#
# End of data contents of table `wp_options`
# --------------------------------------------------------



#
# Delete any existing table `wp_postmeta`
#

DROP TABLE IF EXISTS `wp_postmeta`;


#
# Table structure of table `wp_postmeta`
#

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=447 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_postmeta`
#
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(14, 9, '_edit_last', '1'),
(15, 9, '_edit_lock', '1537428177:1'),
(22, 13, 'wpsl_address', 'VIA ROMA, 195 FC'),
(23, 13, 'wpsl_city', 'MELDOLA'),
(24, 13, 'wpsl_zip', '47014'),
(25, 13, 'wpsl_phone', '0543- 495180'),
(26, 13, 'wpsl_url', 'www.calligaris.com'),
(27, 13, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(28, 13, 'wpsl_lat', '44.142186'),
(29, 13, 'wpsl_lng', '12.063704'),
(30, 13, 'wpsl_country_iso', 'IT'),
(31, 14, 'wpsl_address', 'VIA AUSA, 142 RN '),
(32, 14, 'wpsl_city', 'CERASOLO AUSA'),
(33, 14, 'wpsl_zip', '47852'),
(34, 14, 'wpsl_phone', '0541- 759135'),
(35, 14, 'wpsl_url', 'www.atalmobili.it'),
(36, 14, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(37, 14, 'wpsl_lat', '43.990031'),
(38, 14, 'wpsl_lng', '12.521424'),
(39, 14, 'wpsl_country_iso', 'IT'),
(40, 15, 'wpsl_address', 'VIA DEL BANDO 71/73 RSM'),
(41, 15, 'wpsl_city', 'REPUBBLICA SAN MARINO'),
(42, 15, 'wpsl_zip', '47893'),
(43, 15, 'wpsl_phone', '0549- 902157'),
(44, 15, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(45, 15, 'wpsl_lat', '43.944875'),
(46, 15, 'wpsl_lng', '12.440992'),
(47, 15, 'wpsl_country_iso', 'SM'),
(48, 16, 'wpsl_address', 'VIA CESARE BATTISTA, 49  AG'),
(49, 16, 'wpsl_city', 'CANICATTI'),
(50, 16, 'wpsl_zip', '92024'),
(51, 16, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(52, 16, 'wpsl_lat', '37.358358'),
(53, 16, 'wpsl_lng', '13.850103'),
(54, 16, 'wpsl_country_iso', 'IT'),
(55, 17, 'wpsl_address', 'VIA CULIADA, 121   BL'),
(56, 17, 'wpsl_city', 'FELTRE'),
(57, 17, 'wpsl_zip', '32032'),
(58, 17, 'wpsl_phone', '0439/303543'),
(59, 17, 'wpsl_url', 'www.paulettiarredoecomplementi.it'),
(60, 17, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(61, 17, 'wpsl_lat', '46.01779'),
(62, 17, 'wpsl_lng', '11.88455'),
(63, 17, 'wpsl_country_iso', 'IT'),
(64, 18, 'wpsl_address', 'VIA FOSSOLO, 4-6  BO'),
(65, 18, 'wpsl_city', 'BOLOGNA'),
(66, 18, 'wpsl_zip', '40138'),
(67, 18, 'wpsl_phone', '051/307018'),
(68, 18, 'wpsl_url', 'www.bissoliebissoli.it'),
(69, 18, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(70, 18, 'wpsl_lat', '44.485484'),
(71, 18, 'wpsl_lng', '11.371222'),
(72, 18, 'wpsl_country_iso', 'IT'),
(73, 19, 'wpsl_address', 'VIA ISONZO, 14    BO'),
(74, 19, 'wpsl_city', 'CASALECCHIO DI RENO'),
(75, 19, 'wpsl_zip', '40033'),
(76, 19, 'wpsl_phone', '051/571094'),
(77, 19, 'wpsl_url', 'www.nadaliniarredamenti.it'),
(78, 19, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(79, 19, 'wpsl_lat', '44.485434'),
(80, 19, 'wpsl_lng', '11.277845'),
(81, 19, 'wpsl_country_iso', 'IT'),
(82, 20, 'wpsl_address', 'VIA PROVINCIALE BOLOGNA, 36 BO'),
(83, 20, 'wpsl_city', 'PIEVE DI CENTO'),
(84, 20, 'wpsl_zip', '40066'),
(85, 20, 'wpsl_phone', '0516/861376'),
(86, 20, 'wpsl_url', 'www.marziamelottiroomdesign.it'),
(87, 20, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(88, 20, 'wpsl_lat', '44.703932'),
(89, 20, 'wpsl_lng', '11.305208'),
(90, 20, 'wpsl_country_iso', 'IT'),
(91, 21, 'wpsl_address', 'VIA PROVINCIALE, 67  BS'),
(92, 21, 'wpsl_city', 'ADRO'),
(93, 21, 'wpsl_zip', '25030'),
(94, 21, 'wpsl_phone', '030/7356244'),
(95, 21, 'wpsl_url', 'www.mobiliaurora.it'),
(96, 21, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(97, 21, 'wpsl_lat', '45.625612'),
(98, 21, 'wpsl_lng', '9.952644'),
(99, 21, 'wpsl_country_iso', 'IT'),
(100, 22, 'wpsl_address', 'VIA S. ROCCO,45 - LOC. COSSONE  CR'),
(101, 22, 'wpsl_city', 'OSTIANO'),
(102, 22, 'wpsl_zip', '26032'),
(103, 22, 'wpsl_phone', '0372/840136'),
(104, 22, 'wpsl_url', 'www.arredamentimaestrelli.it'),
(105, 22, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(106, 22, 'wpsl_lat', '45.228262'),
(107, 22, 'wpsl_lng', '10.249735'),
(108, 22, 'wpsl_country_iso', 'IT'),
(109, 23, 'wpsl_address', 'VIA CARPI RAVARINO 1865  MO'),
(110, 23, 'wpsl_city', 'SOZZIGALLI DI SOLIERA'),
(111, 23, 'wpsl_zip', '14010'),
(112, 23, 'wpsl_phone', '059/563813'),
(113, 23, 'wpsl_url', 'www.arredamentivaccari.com'),
(114, 23, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(115, 23, 'wpsl_lat', '44.748572'),
(116, 23, 'wpsl_lng', '10.97644'),
(117, 23, 'wpsl_country_iso', 'IT'),
(118, 24, 'wpsl_address', 'VIA EUGANEA S. BIAGIO 5/A  PD'),
(119, 24, 'wpsl_city', 'TEOLO') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(120, 24, 'wpsl_zip', '35037'),
(121, 24, 'wpsl_phone', '049/9902165'),
(122, 24, 'wpsl_url', 'www.maggioloarredamenti.it'),
(123, 24, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(124, 24, 'wpsl_lat', '45.37233'),
(125, 24, 'wpsl_lng', '11.740911'),
(126, 24, 'wpsl_country_iso', 'IT'),
(127, 25, 'wpsl_address', 'VIALE FRATTI, 42 B   PR'),
(128, 25, 'wpsl_city', 'PARMA'),
(129, 25, 'wpsl_zip', '43121'),
(130, 25, 'wpsl_phone', '0521/272681'),
(131, 25, 'wpsl_url', 'www.sistema-srl.it'),
(132, 25, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(133, 25, 'wpsl_lat', '44.806383'),
(134, 25, 'wpsl_lng', '10.336245'),
(135, 25, 'wpsl_country_iso', 'IT'),
(136, 26, 'wpsl_address', 'VIA ECONOMO, 5/A   TS'),
(137, 26, 'wpsl_city', 'TRIESTE'),
(138, 26, 'wpsl_zip', '34123'),
(139, 26, 'wpsl_phone', '040/307028'),
(140, 26, 'wpsl_url', 'www.mdarredamenti.it'),
(141, 26, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(142, 26, 'wpsl_lat', '45.642136'),
(143, 26, 'wpsl_lng', '13.756894'),
(144, 26, 'wpsl_country_iso', 'IT'),
(145, 27, 'wpsl_address', 'VIA CIVIDALE, 1  UD'),
(146, 27, 'wpsl_city', 'FAEDIS'),
(147, 27, 'wpsl_zip', '33040'),
(148, 27, 'wpsl_phone', '0432/728026'),
(149, 27, 'wpsl_url', 'www.ognistil.com'),
(150, 27, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(151, 27, 'wpsl_lat', '46.15085'),
(152, 27, 'wpsl_lng', '13.348092'),
(153, 27, 'wpsl_country_iso', 'IT'),
(154, 28, 'wpsl_address', 'VIALE MERCATO NUOVO 38  VI'),
(155, 28, 'wpsl_city', 'VICENZA'),
(156, 28, 'wpsl_zip', '36100'),
(157, 28, 'wpsl_phone', '444/560138'),
(158, 28, 'wpsl_url', 'www.iconarredamenti.it'),
(159, 28, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(160, 28, 'wpsl_lat', '45.544808'),
(161, 28, 'wpsl_lng', '11.523335'),
(162, 28, 'wpsl_country_iso', 'IT'),
(163, 29, 'wpsl_address', 'VIA LEONARDO DA VINCI 16/B-C   VR'),
(164, 29, 'wpsl_city', 'SAN GIORGIO IN SALICI'),
(165, 29, 'wpsl_zip', '37060'),
(166, 29, 'wpsl_phone', '0456/095295'),
(167, 29, 'wpsl_url', 'www.sangiorgiomobilisas.it'),
(168, 29, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(169, 29, 'wpsl_lat', '45.42625'),
(170, 29, 'wpsl_lng', '10.792389'),
(171, 29, 'wpsl_country_iso', 'IT'),
(172, 30, 'wpsl_address', 'Via B. Cappuccini, 3 CS'),
(173, 30, 'wpsl_city', 'Pedace'),
(174, 30, 'wpsl_phone', '0984 21961'),
(175, 30, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(176, 30, 'wpsl_lat', '39.275994'),
(177, 30, 'wpsl_lng', '16.336055'),
(178, 30, 'wpsl_country_iso', 'IT'),
(179, 31, 'wpsl_address', 'Via Caprera, 58 CZ'),
(180, 31, 'wpsl_city', 'Catanzaro Lido'),
(181, 31, 'wpsl_phone', '0961 33794-340 8207424'),
(182, 31, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(183, 31, 'wpsl_lat', '38.82964'),
(184, 31, 'wpsl_lng', '16.62732'),
(185, 31, 'wpsl_country_iso', 'IT'),
(186, 32, 'wpsl_address', 'VICO II NAZIONALE , 30  MT'),
(187, 32, 'wpsl_city', 'Matera'),
(188, 32, 'wpsl_zip', '75100'),
(189, 32, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(190, 32, 'wpsl_lat', '40.674719'),
(191, 32, 'wpsl_lng', '16.598184'),
(192, 32, 'wpsl_country_iso', 'IT'),
(193, 33, 'wpsl_address', 'Via Nazionale Catona, 188/E RC'),
(194, 33, 'wpsl_city', 'Reggio Calabria'),
(195, 33, 'wpsl_phone', '0965 600730-329 4034344'),
(196, 33, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(197, 33, 'wpsl_lat', '38.229327'),
(198, 33, 'wpsl_lng', '15.641791'),
(199, 33, 'wpsl_country_iso', 'IT'),
(200, 34, 'wpsl_address', 'VIA A.PREVITALI 41 BG'),
(201, 34, 'wpsl_city', 'Bergamo'),
(202, 34, 'wpsl_zip', '24122'),
(203, 34, 'wpsl_phone', '035 0602437'),
(204, 34, 'wpsl_url', 'http://casatihome.com/'),
(205, 34, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(206, 34, 'wpsl_lat', '45.688821'),
(207, 34, 'wpsl_lng', '9.661736'),
(208, 34, 'wpsl_country_iso', 'IT'),
(209, 35, 'wpsl_address', 'Via Monte Grappa 11 BG'),
(210, 35, 'wpsl_city', 'Osio Sotto'),
(211, 35, 'wpsl_zip', '24046'),
(212, 35, 'wpsl_phone', '035 881178'),
(213, 35, 'wpsl_url', 'https://www.arredolinea.com/'),
(214, 35, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(215, 35, 'wpsl_lat', '45.613345'),
(216, 35, 'wpsl_lng', '9.59366'),
(217, 35, 'wpsl_country_iso', 'IT'),
(218, 36, 'wpsl_address', 'Via S. Giuseppe, 44 CO'),
(219, 36, 'wpsl_city', 'CANTU\'') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(220, 36, 'wpsl_zip', '22063'),
(221, 36, 'wpsl_phone', '031 713605'),
(222, 36, 'wpsl_url', 'http://www.ballabiocucine.it/'),
(223, 36, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(224, 36, 'wpsl_lat', '45.726227'),
(225, 36, 'wpsl_lng', '9.144336'),
(226, 36, 'wpsl_country_iso', 'IT'),
(227, 37, 'wpsl_address', 'VIA G.GARIBALDI, 33 CO'),
(228, 37, 'wpsl_city', 'TURATE'),
(229, 37, 'wpsl_zip', '22708'),
(230, 37, 'wpsl_phone', '02 96481367'),
(231, 37, 'wpsl_url', 'http://www.lamaisondelmobile.it/'),
(232, 37, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(233, 37, 'wpsl_lat', '45.659628'),
(234, 37, 'wpsl_lng', '9.002719'),
(235, 37, 'wpsl_country_iso', 'IT'),
(236, 38, 'wpsl_address', 'VIA ROMA, 4                          LC'),
(237, 38, 'wpsl_city', 'VERCURAGO'),
(238, 38, 'wpsl_zip', '24030'),
(239, 38, 'wpsl_phone', '0341 420465'),
(240, 38, 'wpsl_url', 'http://www.cortieco.it'),
(241, 38, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(242, 38, 'wpsl_lat', '45.815227'),
(243, 38, 'wpsl_lng', '9.42155'),
(244, 38, 'wpsl_country_iso', 'IT'),
(245, 39, 'wpsl_address', 'VIA GARIBALDI, 40                    MB'),
(246, 39, 'wpsl_city', 'CESANO MADERNO'),
(247, 39, 'wpsl_zip', '20811'),
(248, 39, 'wpsl_phone', '0362 506361'),
(249, 39, 'wpsl_url', 'http://salacucine.it/'),
(250, 39, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(251, 39, 'wpsl_lat', '45.629196'),
(252, 39, 'wpsl_lng', '9.152424'),
(253, 39, 'wpsl_country_iso', 'IT'),
(254, 40, 'wpsl_address', 'VIA SARDEGNA, 50                     MI'),
(255, 40, 'wpsl_city', 'MAGNAGO'),
(256, 40, 'wpsl_zip', '20020'),
(257, 40, 'wpsl_phone', '0331 307176'),
(258, 40, 'wpsl_url', 'http://www.studiobidue.com/'),
(259, 40, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(260, 40, 'wpsl_lat', '45.57804'),
(261, 40, 'wpsl_lng', '8.82685'),
(262, 40, 'wpsl_country_iso', 'IT'),
(263, 41, 'wpsl_address', ' Via Galileo Galilei, 12 MI'),
(264, 41, 'wpsl_city', 'Milano'),
(265, 41, 'wpsl_zip', '20124'),
(266, 41, 'wpsl_phone', '02 6379 3281'),
(267, 41, 'wpsl_url', 'http://www.poggenpohl.com/it/trova-uno-studio/italia/poggenpohl-milano/'),
(268, 41, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(269, 41, 'wpsl_lat', '45.480441'),
(270, 41, 'wpsl_lng', '9.196986'),
(271, 41, 'wpsl_country_iso', 'IT'),
(272, 42, 'wpsl_address', 'VIA SEMPIONE, 3                      MI'),
(273, 42, 'wpsl_city', 'POGLIANO MILANESE'),
(274, 42, 'wpsl_zip', '20010'),
(275, 42, 'wpsl_phone', '02 93255892'),
(276, 42, 'wpsl_url', 'http://www.franceschiniarredamenti.it/'),
(277, 42, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(278, 42, 'wpsl_lat', '45.545279'),
(279, 42, 'wpsl_lng', '9.001114'),
(280, 42, 'wpsl_country_iso', 'IT'),
(281, 43, 'wpsl_address', 'VIA PIACENZA, 31/A PC'),
(282, 43, 'wpsl_city', 'CORTEMAGGIORE'),
(283, 43, 'wpsl_zip', '29016'),
(284, 43, 'wpsl_phone', '0523 836188'),
(285, 43, 'wpsl_url', 'http://arredorama.it/'),
(286, 43, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(287, 43, 'wpsl_lat', '45.008357'),
(288, 43, 'wpsl_lng', '9.872545'),
(289, 43, 'wpsl_country_iso', 'IT'),
(290, 44, 'wpsl_address', 'Via C. Battista 106 VA'),
(291, 44, 'wpsl_city', 'Castiglione Olona'),
(292, 44, 'wpsl_zip', '21043'),
(293, 44, 'wpsl_phone', '0331 850230'),
(294, 44, 'wpsl_url', 'https://www.migroarredamenti.it/'),
(295, 44, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(296, 44, 'wpsl_lat', '45.760168'),
(297, 44, 'wpsl_lng', '8.87572'),
(298, 44, 'wpsl_country_iso', 'IT'),
(299, 45, 'wpsl_address', 'Via Bari 4/8 FG'),
(300, 45, 'wpsl_city', 'Lucera'),
(301, 45, 'wpsl_zip', '71036'),
(302, 45, 'wpsl_phone', '0881204605'),
(303, 45, 'wpsl_url', 'www.arredamentipignatelli.it'),
(304, 45, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(305, 45, 'wpsl_lat', '41.50705'),
(306, 45, 'wpsl_lng', '15.331508'),
(307, 45, 'wpsl_country_iso', 'IT'),
(308, 46, 'wpsl_address', 'SP119 Km 3,5 LE'),
(309, 46, 'wpsl_city', 'Monteroni di Lecce'),
(310, 46, 'wpsl_zip', '73047'),
(311, 46, 'wpsl_phone', '0832326424'),
(312, 46, 'wpsl_url', 'www.indencucine.it'),
(313, 46, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(314, 46, 'wpsl_lat', '40.319083'),
(315, 46, 'wpsl_lng', '18.066493'),
(316, 46, 'wpsl_country_iso', 'IT'),
(317, 47, 'wpsl_address', 'C.SO DON MINZONI, 184 AT'),
(318, 47, 'wpsl_city', 'ASTI'),
(319, 47, 'wpsl_zip', '14100') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(320, 47, 'wpsl_phone', '0141-210920'),
(321, 47, 'wpsl_url', 'https://sestosensoarredamenti.com'),
(322, 47, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(323, 47, 'wpsl_lat', '44.899724'),
(324, 47, 'wpsl_lng', '8.193123'),
(325, 47, 'wpsl_country_iso', 'IT'),
(326, 48, 'wpsl_address', 'VIA CUNEO, 97  CN'),
(327, 48, 'wpsl_city', 'BORGO S.DALMAZZO'),
(328, 48, 'wpsl_zip', '12011'),
(329, 48, 'wpsl_phone', '0171-260627'),
(330, 48, 'wpsl_url', 'www.borgodesign.eu/it'),
(331, 48, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(332, 48, 'wpsl_lat', '44.34083'),
(333, 48, 'wpsl_lng', '7.49793'),
(334, 48, 'wpsl_country_iso', 'IT'),
(335, 49, 'wpsl_address', 'VIA BRA, 54 CN'),
(336, 49, 'wpsl_city', 'RORETO DI CHERASCO'),
(337, 49, 'wpsl_zip', '12062'),
(338, 49, 'wpsl_phone', '0172-495139'),
(339, 49, 'wpsl_url', 'https://veroarredamenti.it'),
(340, 49, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(341, 49, 'wpsl_lat', '44.67176'),
(342, 49, 'wpsl_lng', '7.842872'),
(343, 49, 'wpsl_country_iso', 'IT'),
(344, 50, 'wpsl_address', 'VIA A. DE GASPERI, 276 R GE'),
(345, 50, 'wpsl_city', 'CAMPOMORONE'),
(346, 50, 'wpsl_zip', '16014'),
(347, 50, 'wpsl_phone', 'MANASSERO'),
(348, 50, 'wpsl_url', 'www.progettiarredamenti.com'),
(349, 50, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(350, 50, 'wpsl_lat', '44.50666'),
(351, 50, 'wpsl_lng', '8.89255'),
(352, 50, 'wpsl_country_iso', 'IT'),
(353, 51, 'wpsl_address', 'VIA F.LLI CANEPA, 140 GE'),
(354, 51, 'wpsl_city', 'SERRA RICCO\''),
(355, 51, 'wpsl_zip', '16010'),
(356, 51, 'wpsl_phone', '010-752002'),
(357, 51, 'wpsl_url', 'www.mobilifederici.it'),
(358, 51, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(359, 51, 'wpsl_lat', '44.49617'),
(360, 51, 'wpsl_lng', '8.920783'),
(361, 51, 'wpsl_country_iso', 'IT'),
(362, 52, 'wpsl_address', 'VIA L.DA VINCI, 35 NO'),
(363, 52, 'wpsl_city', 'GALLIATE'),
(364, 52, 'wpsl_zip', '28066'),
(365, 52, 'wpsl_phone', '0321-861442'),
(366, 52, 'wpsl_url', 'www.bellettimobili.it'),
(367, 52, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(368, 52, 'wpsl_lat', '45.47814'),
(369, 52, 'wpsl_lng', '8.69889'),
(370, 52, 'wpsl_country_iso', 'IT'),
(371, 53, 'wpsl_address', 'VIA GIOVANNI XXIII, 25 VB'),
(372, 53, 'wpsl_city', 'DOMODOSSOLA'),
(373, 53, 'wpsl_zip', '28845'),
(374, 53, 'wpsl_phone', '0324-45054'),
(375, 53, 'wpsl_url', 'www.centroarredo.com'),
(376, 53, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(377, 53, 'wpsl_lat', '46.12155'),
(378, 53, 'wpsl_lng', '8.292909'),
(379, 53, 'wpsl_country_iso', 'IT'),
(380, 54, 'wpsl_address', 'VIA G. AMENDOLA 8  FI'),
(381, 54, 'wpsl_city', 'SIGNA'),
(382, 54, 'wpsl_phone', '055/875249'),
(383, 54, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(384, 54, 'wpsl_lat', '43.79685'),
(385, 54, 'wpsl_lng', '11.104263'),
(386, 54, 'wpsl_country_iso', 'IT'),
(387, 55, 'wpsl_address', 'VIA L.DA VINCI 228- SOVIGLIANA FI'),
(388, 55, 'wpsl_city', 'VINCI'),
(389, 55, 'wpsl_phone', '0571/509457'),
(390, 55, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(391, 55, 'wpsl_lat', '43.731961'),
(392, 55, 'wpsl_lng', '10.917593'),
(393, 55, 'wpsl_country_iso', 'IT'),
(394, 56, 'wpsl_address', 'VIA GIOVANNI FABBRONI 24/R FI'),
(395, 56, 'wpsl_city', 'FIRENZE'),
(396, 56, 'wpsl_phone', '055/483971'),
(397, 56, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(398, 56, 'wpsl_lat', '43.791079'),
(399, 56, 'wpsl_lng', '11.248771'),
(400, 56, 'wpsl_country_iso', 'IT'),
(401, 57, 'wpsl_address', 'VIA CASONE, 23 - PIANO DI GIOVIA LU'),
(402, 57, 'wpsl_city', 'BORGO A MOZZANO'),
(403, 57, 'wpsl_zip', '55023'),
(404, 57, 'wpsl_phone', '0583- 833307'),
(405, 57, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(406, 57, 'wpsl_lat', '44.017814'),
(407, 57, 'wpsl_lng', '10.525907'),
(408, 57, 'wpsl_country_iso', 'IT'),
(409, 58, 'wpsl_address', 'VIA LIVORNESE EST 23 PI'),
(410, 58, 'wpsl_city', 'PERIGNANO'),
(411, 58, 'wpsl_phone', '0587/616062'),
(412, 58, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(413, 58, 'wpsl_lat', '43.60704'),
(414, 58, 'wpsl_lng', '10.596232'),
(415, 58, 'wpsl_country_iso', 'IT'),
(416, 59, 'wpsl_address', 'STRADA DEL FERRATORE 6 INT 2 SI'),
(417, 59, 'wpsl_city', 'SIENA'),
(418, 59, 'wpsl_phone', '0577/286266'),
(419, 59, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}') ;
INSERT INTO `wp_postmeta` ( `meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(420, 59, 'wpsl_lat', '43.316753'),
(421, 59, 'wpsl_lng', '11.274178'),
(422, 59, 'wpsl_country_iso', 'IT'),
(423, 60, 'wpsl_address', 'VIA PINEROLO 188 TO'),
(424, 60, 'wpsl_city', 'CAVOUR'),
(425, 60, 'wpsl_phone', '0121/6267'),
(426, 60, 'wpsl_url', 'TEMPORANEAMENTE SOSTITUITO MOBILCASA'),
(427, 60, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(428, 60, 'wpsl_lat', '44.80299'),
(429, 60, 'wpsl_lng', '7.37346'),
(430, 60, 'wpsl_country_iso', 'IT'),
(431, 61, 'wpsl_address', 'VIA GENOVA 105 TO'),
(432, 61, 'wpsl_city', 'TORINO'),
(433, 61, 'wpsl_phone', '011/6963985'),
(434, 61, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(435, 61, 'wpsl_lat', '45.028938'),
(436, 61, 'wpsl_lng', '7.668333'),
(437, 61, 'wpsl_country_iso', 'IT'),
(438, 62, 'wpsl_address', 'VIA CROCIFISSO  41/A  AN'),
(439, 62, 'wpsl_city', 'SASSOFERRATO'),
(440, 62, 'wpsl_zip', '60041'),
(441, 62, 'wpsl_phone', '0732/959436'),
(442, 62, 'wpsl_url', 'www.arredamenticamilletti.it'),
(443, 62, 'wpsl_hours', 'a:7:{s:6:"monday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:7:"tuesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:9:"wednesday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"thursday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:6:"friday";a:1:{i:0;s:15:"9:00 AM,5:00 PM";}s:8:"saturday";a:0:{}s:6:"sunday";a:0:{}}'),
(444, 62, 'wpsl_lat', '43.441069'),
(445, 62, 'wpsl_lng', '12.849735'),
(446, 62, 'wpsl_country_iso', 'IT') ;

#
# End of data contents of table `wp_postmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_posts`
#

DROP TABLE IF EXISTS `wp_posts`;


#
# Table structure of table `wp_posts`
#

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_posts`
#
INSERT INTO `wp_posts` ( `ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-09-19 17:59:37', '2018-09-19 15:59:37', 'Benvenuto in WordPress. Questo è il tuo primo articolo. Modificalo o eliminalo, e inizia a creare il tuo blog!', 'Ciao mondo!', '', 'publish', 'open', 'open', '', 'ciao-mondo', '', '', '2018-09-19 17:59:37', '2018-09-19 15:59:37', '', 0, 'https://www.aegvipstore.it/?p=1', 0, 'post', '', 1),
(4, 1, '2018-09-19 18:00:54', '0000-00-00 00:00:00', '', 'Bozza automatica', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-09-19 18:00:54', '0000-00-00 00:00:00', '', 0, 'https://www.aegvipstore.it/?p=4', 0, 'post', '', 0),
(9, 1, '2018-09-19 18:11:05', '2018-09-19 16:11:05', '[wpsl]', 'Rivenditori', '', 'publish', 'closed', 'closed', '', 'rivenditori', '', '', '2018-09-19 18:11:05', '2018-09-19 16:11:05', '', 0, 'https://www.aegvipstore.it/?page_id=9', 0, 'page', '', 0),
(10, 1, '2018-09-19 18:11:05', '2018-09-19 16:11:05', '[wpsl]', 'Rivenditori', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2018-09-19 18:11:05', '2018-09-19 16:11:05', '', 9, 'https://www.aegvipstore.it/index.php/2018/09/19/9-revision-v1/', 0, 'revision', '', 0),
(13, 0, '2018-09-20 09:59:22', '2018-09-20 07:59:22', '', 'MOBILIFICIO RANIERI', '', 'publish', 'closed', 'closed', '', 'mobilificio-ranieri', '', '', '2018-09-20 09:59:22', '2018-09-20 07:59:22', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=mobilificio-ranieri', 0, 'wpsl_stores', '', 0),
(14, 0, '2018-09-20 09:59:23', '2018-09-20 07:59:23', '', 'ATAL MOBILI', '', 'publish', 'closed', 'closed', '', 'atal-mobili', '', '', '2018-09-20 09:59:23', '2018-09-20 07:59:23', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=atal-mobili', 0, 'wpsl_stores', '', 0),
(15, 0, '2018-09-20 09:59:24', '2018-09-20 07:59:24', '', 'CENTRO ARREDAMENTO SAMMARINESE', '', 'publish', 'closed', 'closed', '', 'centro-arredamento-sammarinese', '', '', '2018-09-20 09:59:24', '2018-09-20 07:59:24', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=centro-arredamento-sammarinese', 0, 'wpsl_stores', '', 0),
(16, 0, '2018-09-20 09:59:24', '2018-09-20 07:59:24', '', 'Mobili Giardina Selex di Canicattì', '', 'publish', 'closed', 'closed', '', 'mobili-giardina-selex-di-canicatti', '', '', '2018-09-20 09:59:24', '2018-09-20 07:59:24', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=mobili-giardina-selex-di-canicatti', 0, 'wpsl_stores', '', 0),
(17, 0, '2018-09-20 09:59:25', '2018-09-20 07:59:25', '', 'PAULETTI ARREDO E COMPLEMENTI', '', 'publish', 'closed', 'closed', '', 'pauletti-arredo-e-complementi', '', '', '2018-09-20 09:59:25', '2018-09-20 07:59:25', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=pauletti-arredo-e-complementi', 0, 'wpsl_stores', '', 0),
(18, 0, '2018-09-20 09:59:26', '2018-09-20 07:59:26', '', 'BISSOLI & BISSOLI', '', 'publish', 'closed', 'closed', '', 'bissoli-bissoli', '', '', '2018-09-20 09:59:26', '2018-09-20 07:59:26', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=bissoli-bissoli', 0, 'wpsl_stores', '', 0),
(19, 0, '2018-09-20 09:59:26', '2018-09-20 07:59:26', '', 'NADALINI ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'nadalini-arredamenti', '', '', '2018-09-20 09:59:26', '2018-09-20 07:59:26', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=nadalini-arredamenti', 0, 'wpsl_stores', '', 0),
(20, 0, '2018-09-20 09:59:27', '2018-09-20 07:59:27', '', 'MARZIA MELOTTI ROOM DESIGN', '', 'publish', 'closed', 'closed', '', 'marzia-melotti-room-design', '', '', '2018-09-20 09:59:27', '2018-09-20 07:59:27', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=marzia-melotti-room-design', 0, 'wpsl_stores', '', 0),
(21, 0, '2018-09-20 09:59:28', '2018-09-20 07:59:28', '', 'MOBILI AURORA', '', 'publish', 'closed', 'closed', '', 'mobili-aurora', '', '', '2018-09-20 09:59:28', '2018-09-20 07:59:28', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=mobili-aurora', 0, 'wpsl_stores', '', 0),
(22, 0, '2018-09-20 09:59:28', '2018-09-20 07:59:28', '', 'ARREDAMENTI MAESTRELLI', '', 'publish', 'closed', 'closed', '', 'arredamenti-maestrelli', '', '', '2018-09-20 09:59:28', '2018-09-20 07:59:28', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredamenti-maestrelli', 0, 'wpsl_stores', '', 0),
(23, 0, '2018-09-20 09:59:29', '2018-09-20 07:59:29', '', 'ARREDAMENTI VACCARI', '', 'publish', 'closed', 'closed', '', 'arredamenti-vaccari', '', '', '2018-09-20 09:59:29', '2018-09-20 07:59:29', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredamenti-vaccari', 0, 'wpsl_stores', '', 0),
(24, 0, '2018-09-20 09:59:29', '2018-09-20 07:59:29', '', 'MAGGIOLO ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'maggiolo-arredamenti', '', '', '2018-09-20 09:59:29', '2018-09-20 07:59:29', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=maggiolo-arredamenti', 0, 'wpsl_stores', '', 0),
(25, 0, '2018-09-20 09:59:30', '2018-09-20 07:59:30', '', 'SISTEMA ARREDAMENTI SRL', '', 'publish', 'closed', 'closed', '', 'sistema-arredamenti-srl', '', '', '2018-09-20 09:59:30', '2018-09-20 07:59:30', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=sistema-arredamenti-srl', 0, 'wpsl_stores', '', 0),
(26, 0, '2018-09-20 09:59:31', '2018-09-20 07:59:31', '', 'MD ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'md-arredamenti', '', '', '2018-09-20 09:59:31', '2018-09-20 07:59:31', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=md-arredamenti', 0, 'wpsl_stores', '', 0),
(27, 0, '2018-09-20 09:59:31', '2018-09-20 07:59:31', '', 'OGNISTIL', '', 'publish', 'closed', 'closed', '', 'ognistil', '', '', '2018-09-20 09:59:31', '2018-09-20 07:59:31', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=ognistil', 0, 'wpsl_stores', '', 0),
(28, 0, '2018-09-20 09:59:32', '2018-09-20 07:59:32', '', 'ICONA ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'icona-arredamenti', '', '', '2018-09-20 09:59:32', '2018-09-20 07:59:32', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=icona-arredamenti', 0, 'wpsl_stores', '', 0),
(29, 0, '2018-09-20 09:59:32', '2018-09-20 07:59:32', '', 'SANGIORGIO MOBILI', '', 'publish', 'closed', 'closed', '', 'sangiorgio-mobili', '', '', '2018-09-20 09:59:32', '2018-09-20 07:59:32', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=sangiorgio-mobili', 0, 'wpsl_stores', '', 0),
(30, 0, '2018-09-20 09:59:33', '2018-09-20 07:59:33', '', 'FRM ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'frm-arredamenti', '', '', '2018-09-20 09:59:33', '2018-09-20 07:59:33', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=frm-arredamenti', 0, 'wpsl_stores', '', 0),
(31, 0, '2018-09-20 09:59:34', '2018-09-20 07:59:34', '', 'LEMANI CASA', '', 'publish', 'closed', 'closed', '', 'lemani-casa', '', '', '2018-09-20 09:59:34', '2018-09-20 07:59:34', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=lemani-casa', 0, 'wpsl_stores', '', 0),
(32, 0, '2018-09-20 09:59:34', '2018-09-20 07:59:34', '', 'STIL ARREDO', '', 'publish', 'closed', 'closed', '', 'stil-arredo', '', '', '2018-09-20 09:59:34', '2018-09-20 07:59:34', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=stil-arredo', 0, 'wpsl_stores', '', 0),
(33, 0, '2018-09-20 09:59:35', '2018-09-20 07:59:35', '', 'FORME NUOVE ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'forme-nuove-arredamenti', '', '', '2018-09-20 09:59:35', '2018-09-20 07:59:35', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=forme-nuove-arredamenti', 0, 'wpsl_stores', '', 0),
(34, 0, '2018-09-20 09:59:36', '2018-09-20 07:59:36', '', 'CASATI HOME', '', 'publish', 'closed', 'closed', '', 'casati-home', '', '', '2018-09-20 09:59:36', '2018-09-20 07:59:36', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=casati-home', 0, 'wpsl_stores', '', 0),
(35, 0, '2018-09-20 09:59:36', '2018-09-20 07:59:36', '', 'ARREDOLINEA', '', 'publish', 'closed', 'closed', '', 'arredolinea', '', '', '2018-09-20 09:59:36', '2018-09-20 07:59:36', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredolinea', 0, 'wpsl_stores', '', 0),
(36, 0, '2018-09-20 09:59:37', '2018-09-20 07:59:37', '', 'BALLABIO CUCINE', '', 'publish', 'closed', 'closed', '', 'ballabio-cucine', '', '', '2018-09-20 09:59:37', '2018-09-20 07:59:37', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=ballabio-cucine', 0, 'wpsl_stores', '', 0),
(37, 0, '2018-09-20 09:59:38', '2018-09-20 07:59:38', '', 'LA MAISON DEL MOBILE', '', 'publish', 'closed', 'closed', '', 'la-maison-del-mobile', '', '', '2018-09-20 09:59:38', '2018-09-20 07:59:38', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=la-maison-del-mobile', 0, 'wpsl_stores', '', 0),
(38, 0, '2018-09-20 09:59:38', '2018-09-20 07:59:38', '', 'CORTI & CO.', '', 'publish', 'closed', 'closed', '', 'corti-co', '', '', '2018-09-20 09:59:38', '2018-09-20 07:59:38', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=corti-co', 0, 'wpsl_stores', '', 0),
(39, 0, '2018-09-20 09:59:39', '2018-09-20 07:59:39', '', 'SALA CUCINE', '', 'publish', 'closed', 'closed', '', 'sala-cucine', '', '', '2018-09-20 09:59:39', '2018-09-20 07:59:39', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=sala-cucine', 0, 'wpsl_stores', '', 0),
(40, 0, '2018-09-20 09:59:39', '2018-09-20 07:59:39', '', 'STUDIO BiDue', '', 'publish', 'closed', 'closed', '', 'studio-bidue', '', '', '2018-09-20 09:59:39', '2018-09-20 07:59:39', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=studio-bidue', 0, 'wpsl_stores', '', 0),
(41, 0, '2018-09-20 09:59:40', '2018-09-20 07:59:40', '', 'POGGENPOHL', '', 'publish', 'closed', 'closed', '', 'poggenpohl', '', '', '2018-09-20 09:59:40', '2018-09-20 07:59:40', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=poggenpohl', 0, 'wpsl_stores', '', 0),
(42, 0, '2018-09-20 09:59:41', '2018-09-20 07:59:41', '', 'FRANCESCHINI ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'franceschini-arredamenti', '', '', '2018-09-20 09:59:41', '2018-09-20 07:59:41', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=franceschini-arredamenti', 0, 'wpsl_stores', '', 0),
(43, 0, '2018-09-20 09:59:42', '2018-09-20 07:59:42', '', 'ARREDORAMA', '', 'publish', 'closed', 'closed', '', 'arredorama', '', '', '2018-09-20 09:59:42', '2018-09-20 07:59:42', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredorama', 0, 'wpsl_stores', '', 0),
(44, 0, '2018-09-20 09:59:42', '2018-09-20 07:59:42', '', 'MIGRO ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'migro-arredamenti', '', '', '2018-09-20 09:59:42', '2018-09-20 07:59:42', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=migro-arredamenti', 0, 'wpsl_stores', '', 0),
(45, 0, '2018-09-20 09:59:43', '2018-09-20 07:59:43', '', 'Pignatelli 1930', '', 'publish', 'closed', 'closed', '', 'pignatelli-1930', '', '', '2018-09-20 09:59:43', '2018-09-20 07:59:43', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=pignatelli-1930', 0, 'wpsl_stores', '', 0),
(46, 0, '2018-09-20 09:59:44', '2018-09-20 07:59:44', '', 'Inden Cucine', '', 'publish', 'closed', 'closed', '', 'inden-cucine', '', '', '2018-09-20 09:59:44', '2018-09-20 07:59:44', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=inden-cucine', 0, 'wpsl_stores', '', 0),
(47, 0, '2018-09-20 09:59:44', '2018-09-20 07:59:44', '', 'SESTO SENSO ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'sesto-senso-arredamenti', '', '', '2018-09-20 09:59:44', '2018-09-20 07:59:44', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=sesto-senso-arredamenti', 0, 'wpsl_stores', '', 0),
(48, 0, '2018-09-20 09:59:45', '2018-09-20 07:59:45', '', 'BORGODESIGN', '', 'publish', 'closed', 'closed', '', 'borgodesign', '', '', '2018-09-20 09:59:45', '2018-09-20 07:59:45', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=borgodesign', 0, 'wpsl_stores', '', 0),
(49, 0, '2018-09-20 09:59:46', '2018-09-20 07:59:46', '', 'VE.RO. ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 've-ro-arredamenti', '', '', '2018-09-20 09:59:46', '2018-09-20 07:59:46', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=ve-ro-arredamenti', 0, 'wpsl_stores', '', 0),
(50, 0, '2018-09-20 09:59:46', '2018-09-20 07:59:46', '', 'PROGETTI  ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'progetti-arredamenti', '', '', '2018-09-20 09:59:46', '2018-09-20 07:59:46', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=progetti-arredamenti', 0, 'wpsl_stores', '', 0),
(51, 0, '2018-09-20 09:59:47', '2018-09-20 07:59:47', '', 'MOBILI FEDERICI', '', 'publish', 'closed', 'closed', '', 'mobili-federici', '', '', '2018-09-20 09:59:47', '2018-09-20 07:59:47', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=mobili-federici', 0, 'wpsl_stores', '', 0),
(52, 0, '2018-09-20 09:59:47', '2018-09-20 07:59:47', '', 'MOBILI BELLETTI', '', 'publish', 'closed', 'closed', '', 'mobili-belletti', '', '', '2018-09-20 09:59:47', '2018-09-20 07:59:47', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=mobili-belletti', 0, 'wpsl_stores', '', 0),
(53, 0, '2018-09-20 09:59:48', '2018-09-20 07:59:48', '', 'CENTRO ARREDO', '', 'publish', 'closed', 'closed', '', 'centro-arredo', '', '', '2018-09-20 09:59:48', '2018-09-20 07:59:48', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=centro-arredo', 0, 'wpsl_stores', '', 0),
(54, 0, '2018-09-20 09:59:49', '2018-09-20 07:59:49', '', 'LAMA', '', 'publish', 'closed', 'closed', '', 'lama', '', '', '2018-09-20 09:59:49', '2018-09-20 07:59:49', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=lama', 0, 'wpsl_stores', '', 0),
(55, 0, '2018-09-20 09:59:49', '2018-09-20 07:59:49', '', 'JOLLY ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'jolly-arredamenti', '', '', '2018-09-20 09:59:49', '2018-09-20 07:59:49', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=jolly-arredamenti', 0, 'wpsl_stores', '', 0),
(56, 0, '2018-09-20 09:59:50', '2018-09-20 07:59:50', '', 'AB. ARREDAMENTI BIANCHI', '', 'publish', 'closed', 'closed', '', 'ab-arredamenti-bianchi', '', '', '2018-09-20 09:59:50', '2018-09-20 07:59:50', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=ab-arredamenti-bianchi', 0, 'wpsl_stores', '', 0),
(57, 0, '2018-09-20 09:59:51', '2018-09-20 07:59:51', '', 'FORMA MOBILI', '', 'publish', 'closed', 'closed', '', 'forma-mobili', '', '', '2018-09-20 09:59:51', '2018-09-20 07:59:51', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=forma-mobili', 0, 'wpsl_stores', '', 0),
(58, 0, '2018-09-20 09:59:51', '2018-09-20 07:59:51', '', 'CARDINI HOME DESIGN', '', 'publish', 'closed', 'closed', '', 'cardini-home-design', '', '', '2018-09-20 09:59:51', '2018-09-20 07:59:51', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=cardini-home-design', 0, 'wpsl_stores', '', 0),
(59, 0, '2018-09-20 09:59:52', '2018-09-20 07:59:52', '', 'ARREDAMENTI PROVVEDI', '', 'publish', 'closed', 'closed', '', 'arredamenti-provvedi', '', '', '2018-09-20 09:59:52', '2018-09-20 07:59:52', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredamenti-provvedi', 0, 'wpsl_stores', '', 0),
(60, 0, '2018-09-20 09:59:53', '2018-09-20 07:59:53', '', 'COGNO ARREDAMENTI', '', 'publish', 'closed', 'closed', '', 'cogno-arredamenti', '', '', '2018-09-20 09:59:53', '2018-09-20 07:59:53', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=cogno-arredamenti', 0, 'wpsl_stores', '', 0),
(61, 0, '2018-09-20 09:59:53', '2018-09-20 07:59:53', '', 'SANVIDO ARREDA', '', 'publish', 'closed', 'closed', '', 'sanvido-arreda', '', '', '2018-09-20 09:59:53', '2018-09-20 07:59:53', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=sanvido-arreda', 0, 'wpsl_stores', '', 0),
(62, 0, '2018-09-20 09:59:54', '2018-09-20 07:59:54', '', 'ARREDAMENTI CAMILLETTI', '', 'publish', 'closed', 'closed', '', 'arredamenti-camilletti', '', '', '2018-09-20 09:59:54', '2018-09-20 07:59:54', '', 0, 'https://www.aegvipstore.it/?wpsl_stores=arredamenti-camilletti', 0, 'wpsl_stores', '', 0) ;

#
# End of data contents of table `wp_posts`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_relationships`
#

DROP TABLE IF EXISTS `wp_term_relationships`;


#
# Table structure of table `wp_term_relationships`
#

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_relationships`
#
INSERT INTO `wp_term_relationships` ( `object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(13, 2, 0),
(14, 2, 0),
(15, 2, 0),
(16, 3, 0),
(17, 4, 0),
(18, 4, 0),
(19, 4, 0),
(20, 4, 0),
(21, 4, 0),
(22, 4, 0),
(23, 4, 0),
(24, 4, 0),
(25, 4, 0),
(26, 4, 0),
(27, 4, 0),
(28, 4, 0),
(29, 4, 0),
(30, 5, 0),
(31, 5, 0),
(32, 5, 0),
(33, 5, 0),
(34, 6, 0),
(35, 6, 0),
(36, 6, 0),
(37, 6, 0),
(38, 6, 0),
(39, 6, 0),
(40, 6, 0),
(41, 6, 0),
(42, 6, 0),
(43, 6, 0),
(44, 6, 0),
(45, 7, 0),
(46, 7, 0),
(47, 8, 0),
(48, 8, 0),
(49, 8, 0),
(50, 8, 0),
(51, 8, 0),
(52, 8, 0),
(53, 8, 0),
(54, 9, 0),
(55, 9, 0),
(56, 9, 0),
(57, 9, 0),
(58, 9, 0),
(59, 9, 0),
(60, 9, 0),
(61, 9, 0),
(62, 10, 0) ;

#
# End of data contents of table `wp_term_relationships`
# --------------------------------------------------------



#
# Delete any existing table `wp_term_taxonomy`
#

DROP TABLE IF EXISTS `wp_term_taxonomy`;


#
# Table structure of table `wp_term_taxonomy`
#

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_term_taxonomy`
#
INSERT INTO `wp_term_taxonomy` ( `term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'wpsl_store_category', '', 0, 3),
(3, 3, 'wpsl_store_category', '', 0, 1),
(4, 4, 'wpsl_store_category', '', 0, 13),
(5, 5, 'wpsl_store_category', '', 0, 4),
(6, 6, 'wpsl_store_category', '', 0, 11),
(7, 7, 'wpsl_store_category', '', 0, 2),
(8, 8, 'wpsl_store_category', '', 0, 7),
(9, 9, 'wpsl_store_category', '', 0, 8),
(10, 10, 'wpsl_store_category', '', 0, 1) ;

#
# End of data contents of table `wp_term_taxonomy`
# --------------------------------------------------------



#
# Delete any existing table `wp_termmeta`
#

DROP TABLE IF EXISTS `wp_termmeta`;


#
# Table structure of table `wp_termmeta`
#

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_termmeta`
#

#
# End of data contents of table `wp_termmeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_terms`
#

DROP TABLE IF EXISTS `wp_terms`;


#
# Table structure of table `wp_terms`
#

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_terms`
#
INSERT INTO `wp_terms` ( `term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Senza categoria', 'senza-categoria', 0),
(2, 'APM', 'apm', 0),
(3, 'BUILT-IN', 'built-in', 0),
(4, 'CBELUX', 'cbelux', 0),
(5, 'COMMA', 'comma', 0),
(6, 'EL.GA', 'el-ga', 0),
(7, 'ERGMAN', 'ergman', 0),
(8, 'INELPI', 'inelpi', 0),
(9, 'RACOM', 'racom', 0),
(10, 'TREELLE', 'treelle', 0) ;

#
# End of data contents of table `wp_terms`
# --------------------------------------------------------



#
# Delete any existing table `wp_usermeta`
#

DROP TABLE IF EXISTS `wp_usermeta`;


#
# Table structure of table `wp_usermeta`
#

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_usermeta`
#
INSERT INTO `wp_usermeta` ( `umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:"administrator";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,wpsl_signup_pointer'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:"d0520ea614457a0bf335248c6697d35606838e740a26bc5cc69d47115956b472";a:4:{s:10:"expiration";i:1537545653;s:2:"ip";s:9:"127.0.0.1";s:2:"ua";s:105:"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36";s:5:"login";i:1537372853;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:"ip";s:9:"127.0.0.0";}') ;

#
# End of data contents of table `wp_usermeta`
# --------------------------------------------------------



#
# Delete any existing table `wp_users`
#

DROP TABLE IF EXISTS `wp_users`;


#
# Table structure of table `wp_users`
#

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


#
# Data contents of table `wp_users`
#
INSERT INTO `wp_users` ( `ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Bv6bFsqWBY2a7TxUB1GsjzGf/aVeLf1', 'admin', 'andre.ciccu@softfobia.com', '', '2018-09-19 15:59:36', '', 0, 'admin') ;

#
# End of data contents of table `wp_users`
# --------------------------------------------------------

#
# Add constraints back in and apply any alter data queries.
#

