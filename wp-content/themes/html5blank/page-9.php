<?php get_header(); ?>

<section data-id="3a5d689" class="elementor-element elementor-element-3a5d689 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section jet-parallax-section" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div data-id="1b617a5" class="elementor-element elementor-element-1b617a5 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div data-id="8d4bcd9" class="elementor-element elementor-element-8d4bcd9 elementor-widget elementor-widget-heading" data-element_type="heading.default">
                            <div class="elementor-widget-container">
                                <h1 class="elementor-heading-title elementor-size-default">I NOSTRI RIVENDITORI PREMIUM</h1>
                            </div>
                        </div>

                        <div style="width:100%; float:none; clear:both; text-align:center;">
                            <div class="elementor-text-editor elementor-clearfix">
                                <p>Inserisci nella barra di ricerca la tua zona d’interesse e trova il <strong>Rivenditore Premium AEG</strong> a te più vicino.</p>
                            </div>
                        </div>
                        <div data-id="c7c19b9" class="elementor-element elementor-element-c7c19b9 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">

                            <div class="elementor-widget-container">
                                <div class="elementor-shortcode">

                                    <style>
                                        #wpsl-stores .wpsl-store-thumb {height:45px !important; width:45px !important;}
                                        #wpsl-stores, #wpsl-direction-details, #wpsl-gmap {height:600px !important;}
                                        #wpsl-gmap .wpsl-info-window {max-width:500px !important;}
                                        .wpsl-input label, #wpsl-radius label, #wpsl-category label {width:95px;}
                                        #wpsl-search-input  {width:100%px;}
                                    </style>

	<main role="main">
		<!-- section -->
		<section>

		<?php if (have_posts()): while (have_posts()) : the_post(); ?>

			<!-- article -->
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php the_content(); ?>

				<?php comments_template( '', true ); // Remove if you don't want comments ?>

				<br class="clear">

				<?php edit_post_link(); ?>

			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else: ?>

			<!-- article -->
			<article>

				<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>

        <div data-id="6694302" class="elementor-element elementor-element-6694302 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
            <div class="elementor-widget-container">
                <div class="text-center">
                    <h5 style="text-align:center;"> Vuoi accedere anche tu al programma AEG Better Together e diventare un nostro Rivenditore Premium?</h5>
                    <br>
                    <h6 style="text-align:center;"> Bastano solo pochi semplici passaggi: </h6><br><br>
                    <div style="float:none; clear:both;">
                        <div style="float:left; width:50%">
                            <div style="padding:15px">
                                <span>1)&nbsp;Verifica di possedere i seguenti requisiti:</span>
                                <ul>
                                    <li>Sei un cliente AEG da almeno 12 mesi</li>
                                    <li>Nel tuo negozio è esposta almeno 1 cucina AEG con set della nuova gamma AEG Mastery Range</li>
                                    <li>Ogni anno acquisti un minino di 50 prodotti AEG</li>
                                    <li>Sei disposto a partecipare a training di formazione sui prodotti AEG</li>
                                </ul>
                            </div>
                        </div>
                        <div style="float:left; width:50%">
                            <div style="padding:15px">
                                <span>
                                    2)&nbsp;Contattaci alla mail <a href="mailto:bettertogether.it@electrolux.com">bettertogether.it@electrolux.com</a> per richiedere la verifica dei requisiti e accedi all’esclusivo programma
                                </span>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<!-- /section -->
	</main>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>


<?php get_footer(); ?>
